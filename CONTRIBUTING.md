# Contributing
Please check the Fedora Sway Atomic Docs' ["Contributing"](https://docs.fedoraproject.org/en-US/fedora-sericea/contributing/) page.
